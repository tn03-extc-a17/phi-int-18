/*RGB_TO_GRAYSCALE*/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include "image.h"

int main()
{
const char inputfile[] ="org.bmp";
const char outputfile[] ="output.bmp";
BMPimage image_info =  LFRC_ReadImageFromFile(inputfile);
LFRC_WriteImageToFile(outputfile, image_info);
return 0;
}