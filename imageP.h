#define DATA_OFFSET_OFFSET 0x000A
#define WIDTH_OFFSET 0x0012
#define HEIGHT_OFFSET 0x0016
#define BITS_PER_PIXEL_OFFSET 0x001C

typedef struct image{
         int width;
         int height;
         int  bitsPerPixel;
         unsigned char *data;
}image_s;

image_s *LFRC_ReadImageFromFile( const char *inputfile);
void LFRC_WriteImageToFile(const char *inputfile,const char *outputfile,image_s* img );
void LFRC_free_data(image_s *image);


#endif /* IMAGE_H */